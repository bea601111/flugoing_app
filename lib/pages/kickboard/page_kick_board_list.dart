import 'package:bottom_sheet/bottom_sheet.dart';
import 'package:flugoing_app/components/component_kick_board_item.dart';
import 'package:flugoing_app/model/kick_board_item.dart';
import 'package:flugoing_app/repository/kick_board_into_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageKickBoardList extends StatefulWidget {
  const PageKickBoardList({super.key});

  @override
  State<PageKickBoardList> createState() => _PageKickBoardListState();
}

class _PageKickBoardListState extends State<PageKickBoardList> {
  final _formKey = GlobalKey<FormBuilderState>();
  var kickBoardOptions = ['V-1', 'V-2', 'V-3'];
  List<KickBoardItem> _list = [];

  Future<void> _setInitData() async {
    await KickBoardInfoRepository().getList()
        .then((res) {
      setState(() {
        _list = res.list;
      });
    })
        .catchError((err) {
      print(err);
    });
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _setInitData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {},
        ),
        title: Text('킥보드 리스트', style: TextStyle(fontSize: 20)),
        actions: [
          IconButton(
              onPressed: () {
                showFlexibleBottomSheet(
                  minHeight: 0.4,
                  initHeight: 0.6,
                  maxHeight: 1,
                  context: context,
                  builder: _buildBottomSheet,
                  anchors: [0, 0.5, 1],
                  isSafeArea: true,
                );
              },
              icon: Icon(Icons.list)),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 30),
            Padding(
              padding: const EdgeInsets.only(right: 15, left: 15),
              child: Container(
                height: 230,
                decoration: BoxDecoration(
                    color: Color.fromRGBO(200, 210, 255, 1),
                    borderRadius: BorderRadius.all(Radius.circular(25))),
                child: Padding(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: ListView.builder(
                    itemCount: _list.length,
                    itemBuilder: (BuildContext ctx, int idx) {
                      return ComponentKickBoardItem(
                          kickBoardItem: _list[idx]);
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }


  Widget _buildBottomSheet(
      BuildContext context,
      ScrollController scrollController,
      double bottomSheetOffset,
      ) {
    return Material(
      child: Container(
        child: ListView(
          controller: scrollController,
          children: [
            Container(
              padding: EdgeInsets.only(top: 20),
              alignment: Alignment.center,
              child: Text('필터', style: TextStyle(fontSize: 20)),
            ),
            Container(
              padding: EdgeInsets.only(right: 15),
              alignment: Alignment.centerRight,
              child: Text('닫기', style: TextStyle(fontSize: 20)),
            ),
            Container(
              padding: EdgeInsets.only(left: 15),
              alignment: Alignment.centerLeft,
              child: Text('구입일', style: TextStyle(fontSize: 20)),
            ),
            SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.all(20),
              child: FormBuilderDateTimePicker(
                name: 'startDate',
                initialEntryMode: DatePickerEntryMode.calendarOnly,
                format: DateFormat('yyyy-MM-dd'),
                initialValue: DateTime.now(),
                inputType: InputType.date,
                decoration: InputDecoration(
                  labelText: '시작',
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.close),
                    onPressed: () {
                      _formKey.currentState!.fields['startDate']?.didChange(null);
                    },
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: FormBuilderDateTimePicker(
                name: 'endDate',
                initialEntryMode: DatePickerEntryMode.calendarOnly,
                format: DateFormat('yyyy-MM-dd'),
                initialValue: DateTime.now(),
                inputType: InputType.date,
                decoration: InputDecoration(
                  labelText: '종료',
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.close),
                    onPressed: () {
                      _formKey.currentState!.fields['endDate']?.didChange(null);
                    },
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: FormBuilderDropdown<String>(
                name: 'kickBoardModel',
                decoration: InputDecoration(
                  labelText: '기종',
                ),
                validator: FormBuilderValidators.compose(
                    [FormBuilderValidators.required()]),
                items: kickBoardOptions
                    .map((gender) => DropdownMenuItem(
                  alignment: AlignmentDirectional.center,
                  value: gender,
                  child: Text(gender),
                ))
                    .toList(),
                onChanged: (val) {
                  setState(() {
                    !(_formKey.currentState?.fields['kickBoardModel']?.validate() ??
                        false);
                  });
                },
                valueTransformer: (val) => val?.toString(),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Padding(
              padding: EdgeInsets.only(left: 60, right: 60),
              child: SizedBox(
                height: 40,
                child: FilledButton(
                    onPressed: () {},
                    child: Text('확인', style: TextStyle(fontSize: 20))),
              ),
            ),
          ],
        ),
      ),
    );
  }
}