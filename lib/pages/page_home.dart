import 'package:flugoing_app/components/common/component_appbar_actions.dart';
import 'package:flugoing_app/pages/page_charge_amount.dart';
import 'package:flugoing_app/pages/page_flugoing_pass.dart';
import 'package:flugoing_app/pages/page_info.dart';
import 'package:flugoing_app/pages/member/page_profile.dart';
import 'package:flugoing_app/pages/page_using_history.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class PageHome extends StatefulWidget {
  const PageHome({super.key});

  @override
  State<PageHome> createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(title: " "),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: FloatingActionButton.large(
              heroTag: "btn1",
              child: Icon(Icons.qr_code_scanner_outlined),
              onPressed: () {},
            ),
          ),
          Container(
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: EdgeInsets.only(right: 10, bottom: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    FloatingActionButton.small(
                      heroTag: "btn2",
                      child: Icon(Icons.gps_fixed, color: Colors.white),
                      onPressed: () {},
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    FloatingActionButton.small(
                      heroTag: "btn3",
                      child: Icon(
                        Icons.settings_backup_restore,
                        color: Colors.white,
                      ),
                      onPressed: () {},
                    ),
                  ],
                ),
              )),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Center(
            child: ElevatedButton(
              onPressed: () {
                _showBottomSheet();
              },
              child: Text("Elevated BUTTON"),
            ),
          ),
        ],
      ),
      endDrawer: Drawer(
        elevation: 0.1,
        backgroundColor: Colors.white,
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              margin: EdgeInsets.zero,
              padding: EdgeInsets.only(
                left: 10,
              ),
              child: GestureDetector(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      child: Material(
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                          elevation: 10,
                          child: CircleAvatar(
                            backgroundImage: AssetImage("assets/profile.jpeg"),
                            radius: 50,
                          )),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("박훈희",
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.w700)),
                        Text("010-1234-1234",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.w700)),
                      ],
                    )
                  ],
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => const PageProfile()),
                  );
                },
              ),
            ),
            Container(
              margin: EdgeInsets.zero,
              padding: EdgeInsets.zero,
              child: Padding(
                padding: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("금액 : 1,000,000 원",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w700)),
                    Text("플고잉 패스 잔여 (분) : 45분",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w700)),
                    Text("마일리지 : 1,000",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w700)),
                  ],
                ),
              ),
            ),
            const Divider(
              height: 10,
              thickness: 2,
            ),
            ListTile(
              leading: const Icon(
                Icons.history_outlined,
                size: 40,
              ),
              title: const Text(
                '이용기록',
                style: TextStyle(fontSize: 24),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                      const PageUsingHistory()),
                );
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.electric_scooter_outlined,
                size: 40,
              ),
              title: const Text(
                '풀고잉 패스',
                style: TextStyle(fontSize: 24),
                textAlign: TextAlign.start,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                      const PageFlugoingPass()),
                );
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.payment_outlined,
                size: 40,
              ),
              title: const Text(
                '금액충전',
                style: TextStyle(fontSize: 24),
                textAlign: TextAlign.start,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                      const PageChargeAmount()),
                );
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.help_outline,
                size: 40,
              ),
              title: const Text(
                '고객지원',
                style: TextStyle(fontSize: 24),
                textAlign: TextAlign.start,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => const PageInfo()),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget? floatingButtons() {
    return SpeedDial(
      animatedIcon: AnimatedIcons.menu_close,
      visible: true,
      curve: Curves.bounceOut,
      backgroundColor: Colors.blue.shade900,
      children: [
        SpeedDialChild(
            child: const Icon(Icons.gps_fixed, color: Colors.white),
            label: "내위치",
            labelStyle: const TextStyle(
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 13.0),
            backgroundColor: Colors.blue.shade900,
            labelBackgroundColor: Colors.blue.shade900,
            onTap: () {}),
        SpeedDialChild(
          child: const Icon(
            Icons.settings_backup_restore,
            color: Colors.white,
          ),
          label: "새로고침",
          backgroundColor: Colors.blue.shade900,
          labelBackgroundColor: Colors.blue.shade900,
          labelStyle: const TextStyle(
              fontWeight: FontWeight.w500, color: Colors.white, fontSize: 13.0),
          onTap: () {},
        ),
      ],
    );
  }

  _showBottomSheet() {
    return showModalBottomSheet(
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(25),
          ),
        ),
        builder: (context) {
          return Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(padding: EdgeInsets.all(20),
                    child: Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(color: Colors.red),
                    ),
                  ),
                  Column(
                    children: [
                      Text("AA7BSV"),
                      Row(
                        children: [
                          Icon(Icons.bolt_outlined),
                          Text("data"),
                        ],
                      )
                    ],
                  ),

                  SizedBox(width: 10,),
                  Column(
                    children: [
                      Text("data"),
                      Text("data"),
                    ],
                  ),
                ],
              ),
            ],
          );
        });
  }
}
