import 'package:flugoing_app/components/common/component_appbar_actions.dart';
import 'package:flutter/material.dart';
import 'package:neon_circular_timer/neon_circular_timer.dart';

class PageUsingKickBoard extends StatefulWidget {
  const PageUsingKickBoard({super.key});

  @override
  State<PageUsingKickBoard> createState() => _PageUsingKickBoardState();
}

class _PageUsingKickBoardState extends State<PageUsingKickBoard> {
  final CountDownController controller = new CountDownController();
  int initialDuration = 70;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: '킥보드 사용',
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            decoration: BoxDecoration(color: Colors.red),
            child: Stack(
              children: [
                NeonCircularTimer(
                    onComplete: () {
                      controller.restart();
                    },
                    textFormat: TextFormat.MM_SS,
                    width: 200,
                    controller: controller,
                    duration: 60,
                    strokeWidth: 10,
                    isTimerTextShown: true,
                    neumorphicEffect: true,
                    outerStrokeColor: Colors.grey.shade100,
                    innerFillGradient: LinearGradient(colors: [
                      Colors.greenAccent.shade200,
                      Colors.blueAccent.shade400
                    ]),
                    neonGradient: LinearGradient(colors: [
                      Colors.greenAccent.shade200,
                      Colors.blueAccent.shade400
                    ]),
                    strokeCap: StrokeCap.round,
                    innerFillColor: Colors.black12,
                    backgroudColor: Colors.grey.shade100,
                    neonColor: Colors.blue.shade900),
                Positioned.fill(
                  child: Align(
                    alignment: FractionalOffset.center,
                    child: Text(
                      "",
                      style: Theme.of(context).textTheme.headline3,
                    ),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(40),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  IconButton(
                      icon: Icon(Icons.play_arrow),
                      onPressed: () {
                        controller.resume();
                      }),
                  IconButton(
                      icon: Icon(Icons.pause),
                      onPressed: () {
                        controller.pause();
                      }),
                  IconButton(
                      icon: Icon(Icons.repeat),
                      onPressed: () {
                        controller.restart();
                      }),
                ]),
          )
        ],
      ),
    );
  }
}
