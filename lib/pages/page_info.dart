import 'package:bot_toast/bot_toast.dart';
import 'package:flugoing_app/components/common/component_appbar_actions.dart';
import 'package:flutter/material.dart';

class PageInfo extends StatefulWidget {
  const PageInfo({super.key});

  @override
  State<PageInfo> createState() => _PageInfoState();
}

class _PageInfoState extends State<PageInfo> {
  BackButtonBehavior backButtonBehavior = BackButtonBehavior.none;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(title: "고객센터"),
      body: Padding(
        padding: EdgeInsets.only(right: 30, left: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 30,
            ),
            const Text("무엇을 도와드릴까요 ?",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w800)),
            const SizedBox(
              height: 10,
            ),
            const Divider(
                thickness: 2,
                height: 1,
                color: Colors.grey,
                indent: 10,
                endIndent: 10),
            const SizedBox(
              height: 20,
            ),
            const Text("고객센터 상담시간",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700)),
            const SizedBox(
              height: 40,
            ),
            const Text("전화상담 : 평일 09:00 - 18:00",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                )),
            const SizedBox(
              height: 5,
            ),
            const Padding(
              padding: EdgeInsets.only(left: 5),
              child: Text("점심시간 12:00 - 14:00 / 주말 공휴일 휴무",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500)),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text("채팅상담 : 09:30 - 익일 01:00",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                )),
            const SizedBox(
              height: 5,
            ),
            const Padding(
              padding: EdgeInsets.only(left: 5),
              child: Text("점심시간 12:00 - 14:00 / 공휴일 휴무",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500)),
            ),
            const SizedBox(
              height: 150,
            ),
            Container(
              padding:
                  const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
              width: MediaQuery.of(context).size.width,
              height: 60,
              child: FilledButton.icon(
                label: const Text(
                  "채팅 상담하기",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
                ),
                icon: const Icon(
                  Icons.wechat_rounded,
                  size: 45,
                ),
                style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ))),
                onPressed: () => FlutterDialog('채팅 상담', '채팅 상담하기'),
              ),
            ),
            Container(
              padding:
                  const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
              width: MediaQuery.of(context).size.width,
              height: 60,
              child: FilledButton.icon(
                icon: const Icon(Icons.call, size: 45),
                label: const Text(
                  "전화 상담하기",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
                ),
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.green.shade600),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  )),
                ),
                onPressed: () => FlutterDialog('전화 상담', '전화 상담하기'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  FlutterDialog(String title, String text) {
    showDialog(
        context: context,
        //barrierDismissible - Dialog를 제외한 다른 화면 터치 x
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            // RoundedRectangleBorder - Dialog 화면 모서리 둥글게 조절
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            //Dialog Main Title
            title: Column(
              children: <Widget>[
                Text(title),
              ],
            ),
            //
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(text),
              ],
            ),
            actions: <Widget>[
              TextButton(
                child: Text("확인"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              TextButton(
                child: Text("취소"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }
}
