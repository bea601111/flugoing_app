import 'package:flugoing_app/components/common/component_appbar_actions.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PageChargeAmount extends StatefulWidget {
  const PageChargeAmount({super.key});

  @override
  State<PageChargeAmount> createState() => _PageChargeAmountState();
}

class _PageChargeAmountState extends State<PageChargeAmount> {
  // 변수
  double money = 0;
  var f = NumberFormat.currency(locale: "ko_KR", symbol: "￦");

  // 함수
  _incrementCounter(double amount) {
    setState(() {
      this.money += amount;
    });
  }

  _resetCounter(double amount) {
    setState(() {
      this.money = amount;
    });
  }
  _FlutterDialog() {
    showDialog(
        context: context,
        //barrierDismissible - Dialog를 제외한 다른 화면 터치 x
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            // RoundedRectangleBorder - Dialog 화면 모서리 둥글게 조절
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            //Dialog Main Title
            title: Column(
              children: <Widget>[
                Row(
                  children: [
                    Icon(Icons.info_outline),
                    SizedBox(width: 10),
                    Text("알림"),
                  ],
                ),
              ],
            ),
            //
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "정말로 충전하시겠습니까?",
                ),
              ],
            ),
            actions: <Widget>[
              TextButton(
                child: Text("충전"),
                onPressed: () {
                  Navigator.pop(context);

                },
              ),
              TextButton(
                child: Text("취소"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }
  // 빌드
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: '금액충전',
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            SizedBox(
              height: 40,
            ),
            Container(
              padding: EdgeInsets.only(left: 30),
              child: Row(
                children: [
                  Icon(Icons.monetization_on_rounded),
                  SizedBox(width: 15),
                  Text('충전금액', style: TextStyle(fontSize: 20)),
                ],
              ),
            ),
            SizedBox(height: 20),
            Container(
              height: 40,
              width: 350,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  border: Border.all(color: Colors.grey)),
              padding: EdgeInsets.only(left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    f.format(money),
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  IconButton(
                    onPressed: () => _resetCounter(0),
                    icon: Icon(Icons.cancel, color: Colors.red),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  width: 90,
                  child: FilledButton(
                    onPressed: () => _incrementCounter(1000),
                    child: Text('+ 1,000'),
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ))),
                  ),
                ),
                SizedBox(
                  width: 90,
                  child: FilledButton(
                    onPressed: () => _incrementCounter(3000),
                    child: Text('+ 3,000'),
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ))),
                  ),
                ),
                SizedBox(
                  width: 90,
                  child: FilledButton(
                    onPressed: () => _incrementCounter(5000),
                    child: Text('+ 5,000'),
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ))),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  width: 90,
                  child: FilledButton(
                    onPressed: () => _incrementCounter(10000),
                    child: Text('+ 10,000'),
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ))),
                  ),
                ),
                SizedBox(
                  width: 90,
                  child: FilledButton(
                    onPressed: () => _incrementCounter(30000),
                    child: Text('+ 30,000'),
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ))),
                  ),
                ),
                SizedBox(
                  width: 90,
                  child: FilledButton(
                    onPressed: () => _incrementCounter(50000),
                    child: Text('+ 50,000'),
                    style: ButtonStyle(
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ))),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 25,
            ),
            Padding(
              padding: EdgeInsets.only(left: 40, right: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('충전 후 금액 :',
                      style: TextStyle(fontSize: 23, color: Colors.blue)),
                  Text(f.format(money), style: TextStyle(fontSize: 23)),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: EdgeInsets.only(left: 30),
              child: Row(
                children: [
                  Icon(Icons.credit_card, size: 35),
                  SizedBox(width: 10),
                  Text('결제 전 주의사항', style: TextStyle(fontSize: 18)),
                ],
              ),
            ),
            SizedBox(height: 15),
            Padding(
              padding: EdgeInsets.only(left: 40, right: 40),
              child: Column(
                children: [
                  Text(
                      '※ 자동 충전 금액은 최소 1,000캐시 ~ 최대 50,000 캐시까지 1천 캐시 단위로 입력 가능합니다.',
                      style: TextStyle(fontSize: 15)),
                  SizedBox(height: 15),
                  Text('※ 최초 1회 결제 이후에는 회원님이 설정한 조건대로 결제가 진행됩니다.',
                      style: TextStyle(fontSize: 15)),
                  SizedBox(height: 15),
                  Text('※ 결제 카드를 변경할 시에는 해지 후 카드를 재설정 하셔야 변경이 가능합니다.',
                      style: TextStyle(fontSize: 15)),
                  SizedBox(height: 15),
                ],
              ),
            ),
            SizedBox(height: 40),
            Container(
              padding:
                  const EdgeInsets.only(left: 30, right: 30, top: 5, bottom: 5),
              width: MediaQuery.of(context).size.width,
              height: 60,
              child: FilledButton.icon(
                icon: const Icon(Icons.check_circle, size: 25),
                label: const Text(
                  "충전하기",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
                ),
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.green.shade600),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  )),
                ),
                onPressed: () => _FlutterDialog(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
