import 'package:flugoing_app/components/common/component_appbar_actions.dart';
import 'package:flutter/material.dart';

class PageUsingHistory extends StatelessWidget {
  const PageUsingHistory({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: '사용 내역',
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            _usingHistory('2023-03-31 금', '#2WT388', '16:23 - 16:32 (9분)',
                '2023-03-31 16:32:29', '2,170원'),
          ],
        ),
      ),
    );
  }

  _usingHistory(String date, String useNumber, String useTime,
      String endDataTime, String cost) {
    return Column(
      children: [
        SizedBox(height: 30),
        Container(
          height: 50,
          padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(date, style: TextStyle(fontSize: 20)),
              Text('이용번호' + useNumber),
            ],
          ),
        ),
        Container(
          width: double.infinity,
          padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
          child: Text(
            useTime,
          ),
        ),
        SizedBox(height: 30),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text('결제일시', style: TextStyle(fontSize: 17)),
            Text(endDataTime),
            Text(cost,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600)),
          ],
        ),
        SizedBox(height: 30),
        Divider(
            thickness: 2,
            height: 1,
            color: Colors.grey,
            indent: 10,
            endIndent: 10),
      ],
    );
  }

}
