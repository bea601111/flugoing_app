import 'dart:html';

import 'package:flugoing_app/components/common/component_appbar_actions.dart';
import 'package:flugoing_app/formatter/phone_number_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

import '../../config/config_form_formatter.dart';

class PageJoin extends StatefulWidget {
  const PageJoin({super.key});

  @override
  State<PageJoin> createState() => _PageJoinState();
}

class _PageJoinState extends State<PageJoin> {
  final _formKey = GlobalKey<FormBuilderState>();
  bool _phoneAnth = false;
  bool _usernameDuplication = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(
        title: "회원가입",
      ),
      body: SingleChildScrollView(
        child: FormBuilder(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.only(left: 30, right: 30),
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              SizedBox(
                height: 30,
              ),
              FormBuilderTextField(
                keyboardType: TextInputType.text,
                name: 'memberName',
                decoration: const InputDecoration(
                    labelText: '이름',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(5),
                      ),
                    )),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                  FormBuilderValidators.minLength(5),
                ]),
              ),
              const SizedBox(
                height: 10,
              ),
              FormBuilderTextField(
                name: 'phoneNumber',
                keyboardType: TextInputType.number,
                inputFormatters: [
                  maskPhoneNumberFormatter
                  //13자리만 입력받도록 하이픈 2개+숫자 11개
                ],
                decoration: InputDecoration(
                  labelText: '연락처',
                  hintText: '010-XXXX-XXXX',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  suffixIcon: Container(
                    margin: EdgeInsets.only(right: 5),
                    child: FilledButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.blue.shade600),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        )),
                      ),
                      child: Text(
                        "인증요청",
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w500),
                      ),
                      onPressed: () {
                        setState(() {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text("인증요청"),
                                  content: TextField(
                                    onChanged: (value) {},
                                  ),
                                  actions: [
                                    TextButton(
                                        onPressed: () {
                                          // 인증번호 체크 API
                                        },
                                        child: Text("확인")),
                                    TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text("취소"))
                                  ],
                                );
                              });
                        });
                      },
                    ),
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),
              const SizedBox(
                height: 10,
              ),
              FormBuilderTextField(
                name: 'phoneAuth',
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    suffixIcon: Padding(padding: EdgeInsets.only(top: 12,right: 20),child: Text("00:30")),
                    labelText: '인증번호',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                    )),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                  FormBuilderValidators.minLength(6),
                ]),
              ),
              const SizedBox(
                height: 10,
              ),
              FormBuilderTextField(
                name: 'userName',
                decoration: InputDecoration(
                  labelText: '아이디',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  ),
                  suffixIcon: Container(
                    margin: EdgeInsets.only(right: 5),
                    child: FilledButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.blue.shade600),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        )),
                      ),
                      child: Text(
                        _usernameDuplication ? "확인" : "중복확인",
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w500),
                      ),
                      onPressed: () {
                        setState(() {
                          _usernameDuplication = !_usernameDuplication;
                        });
                        print(_usernameDuplication);
                      },
                    ),
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                  FormBuilderValidators.minLength(5),
                ]),
              ),
              const SizedBox(
                height: 10,
              ),
              FormBuilderTextField(
                name: 'password',
                obscureText: true,
                decoration: const InputDecoration(
                    labelText: '비밀번호',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                    )),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                  FormBuilderValidators.minLength(8),
                ]),
              ),
              const SizedBox(
                height: 10,
              ),
              FormBuilderTextField(
                name: 'rePassword',
                obscureText: true,
                decoration: const InputDecoration(
                    labelText: '비밀번호 확인',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                    )),
                validator: (value) =>
                    _formKey.currentState?.fields['password']?.value != value
                        ? 'No coinciden'
                        : null,
              ),
              // const SizedBox(
              //   height: 10,
              // ),
              // FormBuilderTextField(
              //   name: 'licenseNumber',
              //   keyboardType: TextInputType.number,
              //   inputFormatters: [
              //     maskLicenseFormatter,
              //     //13자리만 입력받도록 하이픈 3개+숫자 12개
              //   ],
              //   decoration: const InputDecoration(
              //       labelText: '면허번증번호',
              //       hintText: '12-11-123456-78',
              //       border: OutlineInputBorder(
              //         borderRadius: BorderRadius.all(Radius.circular(5)),
              //       )),
              //   validator: FormBuilderValidators.compose([
              //     FormBuilderValidators.required(),
              //     FormBuilderValidators.minLength(5),
              //   ]),
              // ),
              const SizedBox(
                height: 20,
              ),
              Container(
                padding: const EdgeInsets.only(
                    left: 10, right: 10, top: 5, bottom: 5),
                width: MediaQuery.of(context).size.width,
                height: 60,
                child: FilledButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(Colors.blue.shade600),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    )),
                  ),
                  child: const Text(
                    "회원가입",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                  ),
                  onPressed: () {},
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}
