import 'package:bot_toast/bot_toast.dart';
import 'package:flugoing_app/config/config_form_validator.dart';
import 'package:flugoing_app/functions/token_lib.dart';
import 'package:flugoing_app/middleware/middleware_login_check.dart';
import 'package:flugoing_app/model/login_request.dart';
import 'package:flugoing_app/pages/member/page_join.dart';
import 'package:flugoing_app/pages/page_home.dart';
import 'package:flugoing_app/repository/member_repository.dart';
import 'package:flugoing_app/widets/loginWidget/wave_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({super.key});

  @override
  State<PageLogin> createState() => _PageLogin();
}

class _PageLogin extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();
  bool _isVisible = true;
  late ScrollController _controller;

  Future<void> _doLogin(LoginRequest loginRequest) async {
    await MemberRepository().doLogin(loginRequest).then((res) {
      TokenLib.setMemberId(res.data.id);
      TokenLib.setMemberName(res.data.memberName);
      MiddlewareLoginCheck().check(context);
      print("성공");
      BotToast.closeAllLoading();
    }).catchError((err) {
      BotToast.closeAllLoading();
      print(err);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    _controller = ScrollController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardVisibilityBuilder(
      builder: (context, isKeyboardVisible) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: Column(children: [
              const SizedBox(
                height: 30,
              ),
              Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height / 2,
                    color: Colors.lightBlue,
                  ),
                  AnimatedPositioned(
                    duration: const Duration(milliseconds: 500),
                    curve: Curves.easeOutQuad,
                    top: isKeyboardVisible
                        ? -MediaQuery.of(context).size.height / 5
                        : 0.0,
                    child: WaveWidget(
                      size: MediaQuery.of(context).size,
                      yOffset: MediaQuery.of(context).size.height / 3.0 + 50,
                      color: Colors.white,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.zero,
                        width: MediaQuery.of(context).size.width,
                        //height: MediaQuery.of(context).size.height / 5,
                        //decoration: BoxDecoration(color: Colors.red),
                        alignment: Alignment.center,
                        child: Image.asset(
                          'assets/img_splash.png',
                          color: Colors.black,
                          fit: BoxFit.fill,
                        ),
                      ),
                      SizedBox(
                        height: 100,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 30),
                        child: Text(
                          'Login',
                          style: TextStyle(
                            fontSize: 40.0,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              FormBuilder(
                key: _formKey,
                child: Container(
                  decoration: BoxDecoration(color: Colors.white),
                  padding: const EdgeInsets.only(right: 30, left: 30),
                  child: Column(children: [
                    SizedBox(
                      height: 30,
                    ),
                    FormBuilderTextField(
                      name: 'userName',
                      decoration: const InputDecoration(
                        labelText: 'ID',
                        labelStyle: TextStyle(color: Colors.blue),
                        filled: true,
                        enabledBorder: UnderlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide.none,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                        prefixIcon: Icon(
                          Icons.account_circle_outlined,
                          color: Colors.blue,
                          size: 20,
                        ),
                      ),
                      validator: validatorOfLoginUsername,
                    ),
                    const SizedBox(height: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        FormBuilderTextField(
                          name: 'password',
                          decoration: InputDecoration(
                            labelText: 'PW',
                            labelStyle: TextStyle(color: Colors.blue),
                            filled: true,
                            enabledBorder: UnderlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              borderSide: BorderSide.none,
                            ),
                            border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                            ),
                            prefixIcon: Icon(
                              Icons.lock_outline,
                              color: Colors.blue,
                              size: 20,
                            ),
                            suffixIcon: GestureDetector(
                              child: _isVisible
                                  ? Icon(Icons.visibility_off,
                                      size: 20, color: Colors.blue)
                                  : Icon(Icons.visibility,
                                      size: 20, color: Colors.blue),
                              onTap: () {
                                setState(() {
                                  _isVisible = !_isVisible;
                                });
                              },
                            ),
                          ),
                          obscureText: _isVisible,
                          validator: validatorOfLoginPassword,
                        ),
                        const SizedBox(height: 10),
                        GestureDetector(
                          child: Text(
                            '비밀번호 찾기',
                            style: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.w500),
                          ),
                          onTap: () {
                            print("비밀번호 찾기");
                          },
                        )
                      ],
                    ),
                    const SizedBox(height: 20),
                    Container(
                      padding: const EdgeInsets.only(
                          left: 10, right: 10, top: 5, bottom: 5),
                      width: MediaQuery.of(context).size.width - 100,
                      height: 60,
                      child: FilledButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.blue.shade600),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          )),
                        ),
                        onPressed: () {
                          if (_formKey.currentState?.saveAndValidate() ??
                              false) {
                            BotToast.showLoading();
                            LoginRequest loginRequest = LoginRequest(
                              _formKey.currentState!.fields['userName']!.value,
                              _formKey.currentState!.fields['password']!.value,
                            );
                            _doLogin(loginRequest);
                          }
                          debugPrint(_formKey.currentState?.value.toString());
                        },
                        child: const Text(
                          "로그인",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(
                          left: 10, right: 10, top: 5, bottom: 5),
                      width: MediaQuery.of(context).size.width - 100,
                      height: 60,
                      child: FilledButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.transparent),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(color: Colors.blue, width: 2.0),
                          )),
                        ),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      const PageJoin()));
                        },
                        child: const Text(
                          "회원가입",
                          style: TextStyle(
                              color: Colors.blue,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    FilledButton(
                      onPressed: () {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    const PageHome()),
                            (route) => false);
                      },
                      child: Text("강제로그인"),
                    ),
                  ]),
                ),
              ),
            ]),
          ),
        );
      },
    );
  }
}
