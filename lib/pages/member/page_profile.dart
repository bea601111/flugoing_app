import 'dart:io';

import 'package:flugoing_app/components/common/component_appbar_actions.dart';
import 'package:flugoing_app/functions/token_lib.dart';
import 'package:flugoing_app/pages/member/page_login.dart';
import 'package:flugoing_app/pages/member/page_profile_fix.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';

import '../../components/common/component_appbar_custom.dart';

class PageProfile extends StatefulWidget {
  const PageProfile({super.key});

  @override
  State<PageProfile> createState() => _PageProfileState();
}

class _PageProfileState extends State<PageProfile> {
  final ImagePicker _picker = ImagePicker();
  XFile? _pickedImage;
  String userName = "홍길동";
  String phoneNumber = "010-1234-1234";
  String licenseNumber = "12-34-123456-78";

  // 카메라, 갤러리에서 이미지 1개 불러오기
  // ImageSource.galley , ImageSource.camera
  void getImage(ImageSource source) async {
    final XFile? image = await _picker.pickImage(source: source);

    setState(() {
      _pickedImage = image;
    });
    if (_pickedImage != null) {
      //_doSaveProfilePath(_pickedImage);
    }
  }

  // AppBar(
  // leading: IconButton(
  // icon: const Icon(Icons.arrow_back),
  // onPressed: () {},
  // ),
  // title: const Text('프로필', style: TextStyle(fontSize: 20)),
  // ),

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: '프로필',
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 30,
          ),
          _pickedImage != null
              ? CircleAvatar(
            maxRadius: (MediaQuery
                .of(context)
                .size
                .width / 2 - 120),
            backgroundImage: FileImage(File(_pickedImage!.path)),
            child: _profileIcon(),
          )
              : CircleAvatar(
            maxRadius: (MediaQuery
                .of(context)
                .size
                .width / 2 - 120),
            backgroundImage: const AssetImage('assets/profile.jpeg'),
            child: _profileIcon(),
          ),
          buildProfile("이름", userName),
          buildProfile("연락처", phoneNumber),
          buildProfile("면허번호", licenseNumber),
          SizedBox(
            height: 10,
          ),
          Container(
            padding:
            const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
            width: MediaQuery
                .of(context)
                .size
                .width - 100,
            height: 60,
            child: FilledButton(
              style: ButtonStyle(
                backgroundColor:
                MaterialStateProperty.all(Colors.blue.shade600),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    )),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (
                            BuildContext context) => const PageProfileFix()));
              },
              child: const Text(
                "회원정보 수정",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
              ),
            ),

          ),
          Container(
            padding:
            const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
            width: MediaQuery
                .of(context)
                .size
                .width - 100,
            height: 60,
            child: FilledButton(
              style: ButtonStyle(
                backgroundColor:
                MaterialStateProperty.all(Colors.blue.shade600),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    )),
              ),
              onPressed: () {
                //
              },
              child: const Text(
                "운전면허 등록",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
              ),
            ),
          ),
          Container(
            padding:
            const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
            width: MediaQuery
                .of(context)
                .size
                .width - 100,
            height: 60,
            child: FilledButton(
              style: ButtonStyle(
                backgroundColor:
                MaterialStateProperty.all(Colors.blue.shade600),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    )),
              ),
              onPressed: () {
                TokenLib.logout(context);
              },
              child: const Text(
                "로그아웃",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Column buildProfile(String title, String textVal) {
    return Column(
      children: [
        Padding(
          padding:
          const EdgeInsets.only(left: 30, right: 30, top: 10, bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(title, style: const TextStyle(fontSize: 20)),
              Text(textVal, style: const TextStyle(fontSize: 20)),
            ],
          ),
        ),
        const Divider(
            thickness: 2,
            height: 1,
            color: Colors.grey,
            indent: 10,
            endIndent: 10),
      ],
    );
  }

  Stack _profileIcon() {
    return Stack(children: [
      Align(
          alignment: Alignment.bottomRight,
          child: CircleAvatar(
            radius: 28,
            backgroundColor: Colors.blue,
            child: IconButton(
              icon: const Icon(
                  Icons.camera_alt_outlined, color: Colors.white, size: 25),
              alignment: Alignment.center,
              onPressed: () {
                _showBottomSheet();
              },
            ),
          ))
    ]);
  }

  _showBottomSheet() {
    return showModalBottomSheet(
      context: context,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(25),
        ),
      ),
      builder: (context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 15,bottom: 15),
              child:
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  TextButton.icon(
                    icon: const Icon(
                      Icons.camera_alt,
                      size: 50,
                    ),
                    label: const Text("Camera"),
                    onPressed: () => getImage(ImageSource.camera),
                  ),
                  TextButton.icon(
                    icon: const Icon(
                      Icons.photo_library,
                      size: 50,
                    ),
                    label: const Text("Gallery"),
                    onPressed: () => getImage(ImageSource.gallery),
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  Future<void> _doSaveProfilePath(XFile? pickedImage) async {
    File tmpFile = File(pickedImage!.path);
    final Directory appDocumentsDir = await getApplicationDocumentsDirectory();

    final String DirectoryPath = appDocumentsDir.path;
    final String fileName = pickedImage!.name;

    print("앱 경로");
    print(appDocumentsDir.path);

    print("파일 경로");
    print(pickedImage!.path);
    print(pickedImage!.name);

    tmpFile = await tmpFile.copy('$DirectoryPath/$fileName');
    print("저장 경로");
    print(tmpFile.path);
  }
}
