import 'package:flugoing_app/components/common/component_appbar_actions.dart';
import 'package:flugoing_app/formatter/phone_number_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageProfileFix extends StatefulWidget {
  const PageProfileFix({super.key});

  @override
  State<PageProfileFix> createState() => _PageProfileFixState();
}

class _PageProfileFixState extends State<PageProfileFix> {
  final _formKey = GlobalKey<FormBuilderState>();
  bool _isVisible = true;
  bool _isReVisible = true;
  bool _isCurrentVisible = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(title: "비밀번호 변경"),
      body: SingleChildScrollView(
        child: FormBuilder(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.only(left: 30, right: 30),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 30,
                  ),

                  Icon(Icons.report_problem_outlined, size: 150,color: Color.fromARGB(255, 200, 0, 0),
                      shadows: <Shadow>[
                        Shadow(color: Colors.grey,blurRadius: 2,offset: Offset(0, 5))
                      ],),
                  SizedBox(
                    height: 10,
                  ),
                  Text("비밀번호를 변경해 주세요!",style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color:Color.fromARGB(255, 230, 0, 0),letterSpacing: 1),),
                  SizedBox(
                    height: 20,
                  ),
                  Text("주기적인 비밀번호 변경을 통해 개인정보를 안전하게 보호하세요."),
                  SizedBox(
                    height: 5,
                  ),
                  Text("아래에 새 비밀번호를 입력해 주세요."),
                  SizedBox(
                    height: 20,
                  ),
                  FormBuilderTextField(
                    keyboardType: TextInputType.text,
                    name: 'currentPassword',
                    obscureText: _isCurrentVisible,
                    decoration: InputDecoration(
                      labelText: '현재 비밀번호',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5),
                        ),
                      ),
                      suffixIcon: GestureDetector(
                        child: _isCurrentVisible
                            ? Icon(Icons.visibility_off,
                            size: 18, color: Colors.blue)
                            : Icon(Icons.visibility,
                            size: 18, color: Colors.blue),
                        onTap: () {
                          setState(() {
                            _isCurrentVisible = !_isCurrentVisible;
                          });
                        },
                      ),
                    ),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(),
                      FormBuilderValidators.minLength(5),
                    ]),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  FormBuilderTextField(
                    keyboardType: TextInputType.text,
                    name: 'password',
                    obscureText: _isVisible,
                    decoration: InputDecoration(
                      labelText: '새 비밀번호',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5),
                        ),
                      ),
                      suffixIcon: GestureDetector(
                        child: _isVisible
                            ? Icon(Icons.visibility_off,
                            size: 18, color: Colors.blue)
                            : Icon(Icons.visibility,
                            size: 18, color: Colors.blue),
                        onTap: () {
                          setState(() {
                            _isVisible = !_isVisible;
                          });
                        },
                      ),
                    ),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(),
                      FormBuilderValidators.minLength(5),
                    ]),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  FormBuilderTextField(
                    keyboardType: TextInputType.text,
                    name: 'rePassword',
                    obscureText: _isReVisible,
                    decoration: InputDecoration(
                      labelText: '비밀번호 확인',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5),
                        ),
                      ),
                      suffixIcon: GestureDetector(
                        child: _isReVisible
                            ? Icon(Icons.visibility_off,
                            size: 18, color: Colors.blue)
                            : Icon(Icons.visibility,
                            size: 18, color: Colors.blue),
                        onTap: () {
                          setState(() {
                            _isReVisible = !_isReVisible;
                          });
                        },
                      ),
                    ),
                    validator: (value) =>
                    _formKey.currentState?.fields['password']?.value !=
                        value
                        ? '일치 하지 않습니다.'
                        : null,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: const EdgeInsets.only(
                        left: 10, right: 10, top: 5, bottom: 5),
                    width: MediaQuery
                        .of(context)
                        .size
                        .width - 100,
                    height: 60,
                    child: FilledButton(
                      style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all(Colors.blue.shade600),
                        shape:
                        MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            )),
                      ),
                      onPressed: () {
                        if (_formKey.currentState?.saveAndValidate() ?? false) {
                          Navigator.pop(context);
                        }
                      },
                      child: const Text(
                        "변경완료",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ]),
          ),
        ),
      ),
    );
  }
}
