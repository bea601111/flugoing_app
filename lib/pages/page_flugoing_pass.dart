import 'package:flugoing_app/components/common/component_appbar_actions.dart';
import 'package:flutter/material.dart';

class PageFlugoingPass extends StatefulWidget {
  const PageFlugoingPass({super.key});

  @override
  State<PageFlugoingPass> createState() => _PageFlugoingPassState();
}

class _PageFlugoingPassState extends State<PageFlugoingPass> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
      title: '풀고잉 패스',
    ),
      body: Column(
        children: [
          Column(
            children: [
              SizedBox(height: 20),
              Container(
                width: double.infinity,
                padding: EdgeInsets.only(left: 30, right: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('1시간 동안, 자유롭게!',
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.w700)),
                    SizedBox(height: 10),
                    Text('최대 51% 할인',
                        style: TextStyle(fontSize: 25, color: Colors.teal)),
                    SizedBox(height: 10),
                    Container(
                      color: Colors.black12,
                      child: Text('잠금해제 무제한 + 이용 무제한 (패스 시간 한정)',
                          style: TextStyle(fontSize: 15)),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Column(
            children: [
              SizedBox(height: 30),
              Container(
                padding: EdgeInsets.all(30),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(color: Colors.lightBlue, width: 2),
                    color: Colors.white),
                alignment: Alignment.center,
                width: 370,
                height: 200,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('1시간 패스', style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600)),
                        SizedBox(height: 5),
                        Text('60분 무제한 이용', style: TextStyle(fontSize: 20)),
                        SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('~ 44%',
                                style: TextStyle(
                                    fontSize: 25, color: Colors.teal)),
                            SizedBox(width: 30),
                            Text('4,900원', style: TextStyle(fontSize: 25)),
                          ],
                        ),
                        SizedBox(height: 5),
                        Text('8,800원',
                            style: TextStyle(
                                color: Colors.grey,
                                decoration: TextDecoration.lineThrough)),
                      ],
                    ),
                    SizedBox(
                      child: IconButton(
                        onPressed: () {},
                        icon: Icon(Icons.arrow_forward_ios),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 30),
              Container(
                padding: EdgeInsets.all(30),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(color: Colors.lightBlue, width: 2),
                    color: Colors.white),
                alignment: Alignment.center,
                width: 370,
                height: 200,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('2시간 패스', style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600)),
                        SizedBox(height: 5),
                        Text('120분 무제한 이용', style: TextStyle(fontSize: 20)),
                        SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('~ 51%',
                                style: TextStyle(
                                    fontSize: 25, color: Colors.teal)),
                            SizedBox(width: 30),
                            Text('8,000원', style: TextStyle(fontSize: 25)),
                          ],
                        ),
                        SizedBox(height: 5),
                        Text('16,600원',
                            style: TextStyle(
                                color: Colors.grey,
                                decoration: TextDecoration.lineThrough)),
                      ],
                    ),
                    SizedBox(
                      child: IconButton(
                        onPressed: () {},
                        icon: Icon(Icons.arrow_forward_ios),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
