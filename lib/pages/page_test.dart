import 'dart:async';

import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

import '../components/common/component_appbar_actions.dart';

class PageTest extends StatefulWidget {
  const PageTest({super.key});

  @override
  State<PageTest> createState() => _PageTestState();
}

class _PageTestState extends State<PageTest> {
  late Timer _timer;
  int _percentCounter = 0;
  double _percentValue = 0.0;
  bool isTimer = false;
  int sec = 0;
  int min = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _timer.cancel();
  }

  String _format(int seconds) {
    var duration = Duration(seconds: seconds);
    return duration.toString().split('.').first.substring(2, 7);
  }

  void _startTimer() {
    if (!isTimer) {
      isTimer = true;
      _timer = Timer.periodic(Duration(milliseconds: 100), (timer) {
        setState(() {
          sec++;
          if (_percentCounter >= 59) {
            _percentCounter = 0;
          } else {
            _percentCounter++;
          }

          _percentValue = ((100/60) * _percentCounter) / 100;
          print(_percentCounter);
        });
      });
    }
  }

  void _isTimer() {
    print(isTimer.toString());
  }

  void _stopTimer() {
    isTimer = false;
    _timer.cancel();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(
        title: "테스트 페이지",
      ),
      body: Column(
        children: [
          SizedBox(
            height: 50,
          ),
          Center(
            child:
            CircularPercentIndicator(
                radius: 100.0,
                animation: true,
                animationDuration: 500,
                lineWidth: 20.0,
                percent: _percentValue,
                reverse: false,
                startAngle: 0.0,
                animateFromLastPercent: true,
                circularStrokeCap: CircularStrokeCap.round,
                backgroundColor: Colors.grey,
                linearGradient: const LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  tileMode: TileMode.clamp,
                  stops: [0.0, 1.0],
                  colors: <Color>[
                    Colors.greenAccent,
                    Colors.blueAccent,
                  ],
                ),
              center: Text(_format(sec)),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          ElevatedButton(
              onPressed: () {
                _startTimer();
              },
              child: Text("START")),
          SizedBox(
            height: 10,
          ),
          ElevatedButton(
              onPressed: () {
                _stopTimer();
              },
              child: Text("STOP")),
          SizedBox(
            height: 10,
          ),
          ElevatedButton(
              onPressed: () {
                _isTimer();
              },
              child: Text("inTimer")),
        ],
      ),
    );
  }
}
