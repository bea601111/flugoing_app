import 'package:bot_toast/bot_toast.dart';
import 'package:flugoing_app/login_check.dart';
import 'package:flugoing_app/pages/kickboard/page_kick_board_list.dart';
import 'package:flugoing_app/pages/member/page_join.dart';
import 'package:flugoing_app/pages/member/page_profile_fix.dart';
import 'package:flugoing_app/pages/page_charge_amount.dart';
import 'package:flugoing_app/pages/page_flugoing_pass.dart';
import 'package:flugoing_app/pages/page_home.dart';
import 'package:flugoing_app/pages/member/page_profile.dart';
import 'package:flugoing_app/pages/page_info.dart';
import 'package:flugoing_app/pages/page_test.dart';
import 'package:flugoing_app/pages/page_using_history.dart';
import 'package:flugoing_app/pages/page_using_kick_board.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

import 'pages/member/page_login.dart';


void main() {
  //SystemChrome.setSystemUIOverlayStyle(
    //SystemUiOverlayStyle(
      //systemNavigationBarColor:
      //SystemUiOverlayStyle.dark.systemNavigationBarColor,
    //),
  //);

  //WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  //FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      localizationsDelegates: [
        FormBuilderLocalizations.delegate,
        ...GlobalMaterialLocalizations.delegates,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: FormBuilderLocalizations.supportedLocales,

      builder: BotToastInit(),
      navigatorObservers: [BotToastNavigatorObserver()],

      // home: LoginCheck(),         // 로그인 체크
      // home: PageLogin(),          // 로그인
      // 비밀번호 찾기
      // home: PageJoin(),           // 회원가입

      // home: PageProfile(),        // 프로필
      // home: PageProfileFix(),     // 비밀번호 변경
      // 운전면허등록

      // home: PageHome(),           // 홈
       home: PageUsingHistory(),   // 이용 기록
      // 이용기록 상세내역
      // home: PageFlugoingPass(),   // 패스
      // home: PageChargeAmount(),   // 금액충전
      // home: PageInfo(),           // 고객지원

      // home: PageUsingKickBoard(), // 킥보드 사용 - 작업중
      // 킥보드 종료

      // 관리자 회원리스트
      // 관리자 회원 리스트 상세
      // home: PageKickBoardList(),  // 관리자 킥보드 리스트 (확인 하여야함)
      // 관리자 킥보드 리스트 상세
      // home: PageStatistical(),    // 관리자 매출통계

      //home: PageTest(),             // 테스트

    );
  }
}

