import 'package:dio/dio.dart';
import 'package:flugoing_app/config/config_api.dart';
import 'package:flugoing_app/model/kick_board_item_result.dart';

class KickBoardInfoRepository{

    Future<KickBoardItemResult> getList () async {
      const String baseUrl ='$apiUri/kick-board-info/all-kick-board';

      Dio dio = Dio();

      final response = await dio.get(
          baseUrl,
          options: Options(
              followRedirects: false,
              validateStatus: (status) {
                return status == 200;
              }
          )
      );

      return KickBoardItemResult.fromJson(response.data);
    }
}