
import 'package:dio/dio.dart';
import 'package:flugoing_app/config/config_api.dart';
import 'package:flugoing_app/model/common_result.dart';
import 'package:flugoing_app/model/login_request.dart';
import 'package:flugoing_app/model/login_result.dart';
import 'package:flugoing_app/model/member_request.dart';


class MemberRepository {
  Future<LoginResult> doLogin(LoginRequest request) async {
    const String baseUrl = '$apiUri/member/login';

    Dio dio = Dio();

    final response = await dio.post(
      baseUrl,
      data: request.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return LoginResult.fromJson(response.data);
  }

  Future<CommonResult> setMember(MemberRequest request) async {
    const String baseUrl = '$apiUri/member/data';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }
}