import 'package:flugoing_app/functions/token_lib.dart';
import 'package:flugoing_app/pages/page_home.dart';
import 'package:flugoing_app/pages/member/page_login.dart';
import 'package:flutter/material.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    int? memberId = await TokenLib.getMemberId();
    
    if (memberId == null) {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin()), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageHome()), (route) => false);
    }
  }
}