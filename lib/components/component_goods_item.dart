import 'package:flutter/material.dart';

class ComponentGoodsItem extends StatelessWidget {
  const ComponentGoodsItem({super.key, required this.imgUrl, required this.goodsName, required this.goodsPrice, required this.voidCallback});

  final String imgUrl;
  final String goodsName;
  final double goodsPrice;
  final VoidCallback voidCallback;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Image.network(
            imgUrl,
            width: 100,
            height: 100,
            fit: BoxFit.fill,
          ),
          Text(goodsName),
          Text(goodsPrice.toString()),
          OutlinedButton(
              onPressed: voidCallback,
              child: const Text('담기')
          ),
        ],
      ),
    );
  }
}
