import 'package:flugoing_app/model/kick_board_item.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ComponentKickBoardItem extends StatelessWidget {
  const ComponentKickBoardItem({super.key, required this.kickBoardItem});

  final KickBoardItem kickBoardItem;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text("상태 : ${kickBoardItem.kickBoardStatus}"),
        Text("모델명 : ${kickBoardItem.modelName}"),
        Text("구입일 : ${kickBoardItem.dateBuy}"),
        Text("가격기준 : ${kickBoardItem.priceBasis}"),
        Text("현제 위치 : ${kickBoardItem.posX}, ${kickBoardItem.posY}"),
        Text("사용여부 : ${kickBoardItem.isUse ? "예" : "아니오"}"),
      ],
    );
  }
}
