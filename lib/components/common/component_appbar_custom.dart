import 'package:flutter/material.dart';

class ComponentAppbarCustom extends StatelessWidget {
  final VoidCallback? onPressed;
  final String title;

  const ComponentAppbarCustom(
      {super.key, required this.title, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      const SizedBox(
        height: 30,
      ),
      IconButton(
        icon: const Icon(
          Icons.arrow_back,
          size: 50,
        ),
        onPressed: onPressed,
      ),
      const SizedBox(
        height: 30,
      ),
      Padding(
        padding: EdgeInsets.only(left: 10),
        child : Text(title, style: const TextStyle(fontSize: 30)),
      ),
      const SizedBox(
        height: 10,
      ),
      const Divider(
          thickness: 2,
          height: 1,
          color: Colors.grey,
          indent: 10,
          endIndent: 10),
      const SizedBox(
        height: 10,
      ),
    ]);
  }
}
