
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

var maskLicenseFormatter = MaskTextInputFormatter(
    mask: 'S#-##-######-##',
    filter: { "#": RegExp(r'[0-9]') , "S":  RegExp(r'[1-2]')},
    type: MaskAutoCompletionType.lazy
);




var maskPhoneNumberFormatter = MaskTextInputFormatter(
    mask: '010-####-####',
    filter: { "#": RegExp(r'[0-9]')},
    type: MaskAutoCompletionType.lazy
);