import 'package:flutter/services.dart';

class PhoneNumberFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    var text = newValue.text;

    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    var buffer = StringBuffer();
    var butter0fCount11 = StringBuffer();

    for (int i = 0; i < text.length; i++) {
      buffer.write(text[i]);
      var nonZeroIndex = i + 1;
      if (nonZeroIndex <= 3) {
        if (nonZeroIndex % 3 == 0 && nonZeroIndex != text.length) {
          buffer.write('-'); // Add double spaces.
        }
      } else {
        if (nonZeroIndex % 6 == 0 &&
            nonZeroIndex != text.length &&
            nonZeroIndex > 3) {
          buffer.write('-');
        }
      }
    }

    var string = buffer.toString();

    if(text.length >= 11){
      for (int i = 0; i < text.length; i++) {
        butter0fCount11.write(text[i]);
        var nonZeroIndex = i + 1;
        if (nonZeroIndex <= 3) {
          if (nonZeroIndex % 3 == 0 && nonZeroIndex != text.length) {
            butter0fCount11.write('-'); // Add double spaces.
          }
        } else {
          if (nonZeroIndex % 7 == 0 &&
              nonZeroIndex != text.length &&
              nonZeroIndex > 4) {
            butter0fCount11.write('-');
          }
        }
      }
      string = butter0fCount11.toString();
    }

    return newValue.copyWith(
        text: string,
        selection: TextSelection.collapsed(offset: string.length));
  }
}
