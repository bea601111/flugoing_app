class KickBoardItem {
  int id;
  String kickBoardStatus;
  String modelName;
  String priceBasis;
  String dateBuy;
  double posX;
  double posY;
  bool isUse;

  KickBoardItem(this.id, this.isUse, this.kickBoardStatus, this.modelName,
      this.posX, this.posY, this.priceBasis, this.dateBuy);

  factory KickBoardItem.fromJson(Map<String, dynamic> json) {
    return KickBoardItem(
      json['id'],
      json['isUse'] as bool,
      json['kickBoardStatus'],
      json['modelName'],
      json['posX'],
      json['posY'],
      json['priceBasis'],
      json['dateBuy'],
    );
  }
}
