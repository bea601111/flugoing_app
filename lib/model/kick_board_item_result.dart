import 'package:flugoing_app/model/kick_board_item.dart';

class KickBoardItemResult {
  int code;
  int currentPage;
  bool isSuccess;
  List<KickBoardItem> list;
  String msg;
  int totalItemCount;
  int totalPage;

  KickBoardItemResult(this.isSuccess, this.code, this.msg, this.totalItemCount, this.totalPage, this.currentPage, this.list);

  factory KickBoardItemResult.fromJson(Map<String, dynamic> json) {
    return KickBoardItemResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] == null ? [] : (json['list'] as List).map((e) => KickBoardItem.fromJson(e)).toList(),
    );
  }
}
