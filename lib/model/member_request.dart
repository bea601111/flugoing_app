class MemberRequest {
  String username;
  String password;
  String memberName;
  String phoneNumber;
  String licenseNumber;
  String birthDay;

  MemberRequest(this.username, this.password, this.memberName, this.phoneNumber, this.licenseNumber, this.birthDay);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['username'] = this.username;
    data['password'] = this.password;
    data['memberName'] = this.memberName;
    data['phoneNumber'] = this.phoneNumber;
    data['licenseNumber'] = this.licenseNumber;
    data['birthDay'] = this.birthDay;

    return data;
  }
}