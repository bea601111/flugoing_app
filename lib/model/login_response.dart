class LoginResponse {
  int id;
  String memberName;

  LoginResponse(this.id, this.memberName);

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
        json['id'],
        json['memberName']
    );
  }
}