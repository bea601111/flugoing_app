

import 'package:flugoing_app/model/login_response.dart';

class LoginResult {
  bool isSuccess;
  int code;
  String msg;
  LoginResponse data;

  LoginResult(this.isSuccess, this.code, this.msg, this.data);

  factory LoginResult.fromJson(Map<String, dynamic> json) {
    return LoginResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      LoginResponse.fromJson(json['data'])
    );
  }
}