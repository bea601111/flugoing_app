import 'package:bot_toast/bot_toast.dart';
import 'package:flugoing_app/pages/member/page_login.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TokenLib {
  static Future<int?> getMemberId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt('memberId');
  }

  static Future<String?> getMemberName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberName');
  }

  static void setMemberId(int memberId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt('memberId', memberId);
  }

  static void setMemberName(String memberName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberName', memberName);
  }

  static void logout(BuildContext context) async {
    BotToast.closeAllLoading();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
    
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => const PageLogin()),
            (route) => false
    );
  }
}